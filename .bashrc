vb6_code_gen() {
    # $1 is the database
    # $2 is the jinja template
    # $3 is the alias
 python ~/scrapps/DALCodeGen/fields.py \
     -u sfdc \
     -p Technique\! \
     -s UKLESQL02 \
     -d $1 \
     -t $2 \
     | python ~/scrapps/DALCodeGen/code_gen.py \
         -ta $2 \
         -c $2 \
         -a $3 \
         -t ~/scrapps/DALCodeGen/VB6Model.jinja
}

alias ls="ls --color=always"
alias gitka='gitk --all &'
alias grhh='git reset --hard HEAD'
alias gd='git diff'
alias gs='git status'
alias vb6_lint='find -maxdepth 1 -type f -regex ".*\(cls\|frm\|bas\)" -exec ~/Scripts/VB6Lint.sh {} \;'
alias vb6_autolog='find -type f -regex ".*\(cls\|frm\|bas\)" -exec python ~/Scripts/VB6AutoLog.py -o tmp -f {} \; -exec mv tmp {} \;'

set_db_old() {
    sed -e "s/02/$1/" -e "s/DEV_/$2/ " -e "s/EstimateCalculationEngine/CalcEngine$3/" /c/Windows/BaseTechnique.ini > tmp;
    mv tmp /c/Windows/Technique.ini;
    sed -e "s/02/$1/" -e "s/DEV_/$2/ " /c/Windows/Base51Web.Config > tmp;
    cp tmp /h/scripts/CalcEngine5.1.6/Web.config;
    sed -e "s/02/$1/" -e "s/DEV_/$2/ " /c/Windows/Base52Web.Config > tmp;
    cp tmp /h/scripts/CalcEngine5.2.1/Web.config;
    rm tmp;
}

discard_case_changes() {
    # Function to discard any case only changes that haven't been staged.

    # Create a backup of the current changes
    BACKUPFILE="$( date +%Y%m%d%H%M%S.backup.patch )"
    git diff > $BACKUPFILE

    # For each file that has been changed
    for f in $( git diff --name-only --ignore-submodules=all); do
        
        # skip if the file is being removed
        if [[ ! -f "$f" ]]; then
            continue
        fi

        # Create a case insensitive patch
        # git show :$f gets the unmodified version of the file
        git show :$f | diff -uiw - $f > temp.patch;
        
        # Reset the file
        git checkout -- $f;
        
        # Apply the case insensitive patch, hence discarding case only changes
        git apply --whitespace=nowarn temp.patch;
        
        # git apply doesn't respect core.autocrlf so replace all LF with CRLF
        # $ matches the end of line
        # '\r' expands to CRLF in a bash shell
        sed s/$/'\r'/ $f > temp.txt;
        mv temp.txt $f;
        
        # clean up temporary files
        rm temp.patch;
    done

    echo "If everything is as expected you can delete $BACKUPFILE.
    If things have gone wrong you can revert to your previous state by executing;

        git reset --hard HEAD
        git apply $BACKUPFILE"

}

gsuf() {
    git submodule update -f
}

gdv() {
    git diff > ~/tmp.diff && gvim ~/tmp.diff
}

fcheckout() {
    git checkout -f $1
    git pull
    git submodule update -f
}

vb6start() {
    if [ -e *.vbp ];
    then
        start *.vbp /c $1
    fi

    if [ -e *.VBP ];
    then
        start *.VBP /c $1
    fi
}
source ~/Envy/etc/config.sh
