colo evening
cd C:\Users\Administrator
syntax on

filetype plugin indent on
set expandtab softtabstop=4 smartindent shiftwidth=4 guifont=Consolas hidden
set colorcolumn=80 number 
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
"set foldmethod=indent
"nnoremap <space> za
"vnoremap <space> zf
" Automatically enable the preview function with vim starting
let g:AutoPreview_enabled =1 
" make you could press F5 key to enable or disable the preview window, you can 
" also set to other favorite hotkey here
nnoremap <F5> :!python %<Return>
inoremap <F5> <ESC>:!python %<Return>
" set the time(ms) break to refresh the preview window, I recommend 500ms~1000ms with good experience
set updatetime=500
set hlsearch
set ignorecase
set smartcase
set incsearch
